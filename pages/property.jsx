import Image from "next/image";
import React from "react";
import propertyImg from "../public/assets/projects/printer_landscape.jpg";
import { RiRadioButtonFill } from "react-icons/ri";
import Link from "next/link";

const property = () => {
  return (
    <div className="w-full">
      <div className="w-screen h-[50vh] relative">
        <div className="absolute top-0 left-0 w-full h-[50vh] bg-black/70 z-10" />
        <Image
          className="absolute z-1"
          layout="fill"
          objectFit="cover"
          src={propertyImg}
          alt="/"
        />
        <div className="absolute top-[70%] max-w-[1240px] w-full left-[50%] right-[50%] translate-x-[-50%] translate-y-[-50%] text-white z-10 p-2">
          <h2 className="py-2">3D House Printer</h2>
          <h3>Mechatronics Engineering / C++ / KiCAD / Radio Technology 2.4 Ghz/ </h3>
        </div>
      </div>

      <div className="max-w-[1240px] mx-auto p-2 grid md:grid-cols-5 gap-8 py-8">
        <div className="col-span-4">
          <p>Project</p>
          <h2>Overview</h2>
         

          <p>
            In developing the 3D House Printer, I have incorporated a range of
            cutting-edge technologies to ensure its optimal performance. With a
            background in mechatronics engineering, I have seamlessly integrated
            mechanical, electrical, and computer systems to create a robust and
            precise machine. The printer's microcontroller is programmed using
            the versatile C++ language, enabling efficient control and
            coordination of its operations. To guarantee quality, I have
            implemented thorough unit testing and followed continuous
            integration and delivery practices throughout the development
            process. Moreover, I have employed a creative solution for device
            operation, utilizing radio communication and hacking PlayStation 4
            (PS4) gamepads. By customizing and repurposing these gamepads, I
            have created an intuitive control system that allows users to easily
            operate the 3D House Printer. This innovative approach enhances the
            user experience, making the printer's operation accessible and
            enjoyable.
          </p>
          {/* <a
            href="https://github.com/fireclint/property-finder"
            target="_blank"
            rel="noreferrer"
          >
            <button className="px-8 py-2 mt-4 mr-8">Code</button>
          </a> */}
          <a
            href="https://www.upwork.com/freelancers/~0107fd79d2892839cb"
            target="_blank"
            rel="noreferrer"
          >
            <button className="px-8 py-2 mt-4">Demo</button>
          </a>
        </div>
        <div className="col-span-4 md:col-span-1 shadow-xl shadow-gray-400 rounded-xl py-4">
          <div className="p-2">
            <p className="text-center font-bold pb-2">Technologies</p>
            <div className="grid grid-cols-3 md:grid-cols-1">
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> C++
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Mechatronics Engineering
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> STM32 (Arm Cortex)
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Radio Technology (2.4 Ghz)
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> PS4 Game pads
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Stepper Motors
              </p>
            </div>
          </div>
        </div>
        <Link href="/#projects">
          <p className="underline cursor-pointer">Back</p>
        </Link>
      </div>
    </div>
  );
};

export default property;
