import Image from "next/image";
import React from "react";
import cryptoImg from "../public/assets/projects/LED_scanner_landscape.png";
import { RiRadioButtonFill } from "react-icons/ri";
import Link from "next/link";

const crypto = () => {
  return (
    <div className="w-full">
      <div className="w-screen h-[50vh] relative">
        <div className="absolute top-0 left-0 w-full h-[50vh] bg-black/70 z-10" />
        <Image
          className="absolute z-1"
          layout="fill"
          objectFit="cover"
          src={cryptoImg}
          alt="/"
        />
        <div className="absolute top-[70%] max-w-[1240px] w-full left-[50%] right-[50%] translate-x-[-50%] translate-y-[-50%] text-white z-10 p-2">
          <h2 className="py-2">LED Car RPM Indicator</h2>
          <h3>Addressable LEDs / C++ / OBDII Diagnostic Car port</h3>
        </div>
      </div>

      <div className="max-w-[1240px] mx-auto p-2 grid md:grid-cols-5 gap-8 py-8">
        <div className="col-span-4">
          <p>Project</p>
          <h2>Overview</h2>
          <p>
            In creating the LED Car RPM indicator, I utilized a combination of
            technologies to provide an efficient and visually striking solution.
            Leveraging my programming skills in C++, I developed a robust
            software system that interfaces with the car's OBDII port. This
            allowed me to retrieve real-time RPM data directly from the engine.
            To bring the RPM data to life, I employed addressable LEDs, which
            are capable of individual control and vibrant lighting effects. With
            the RPM data at hand, I devised an algorithm that mapped the RPM
            range to the corresponding LED strip lighting. As the RPM increased,
            the algorithm dynamically illuminated more LEDs, creating a visually
            intuitive representation of the engine's speed. The higher the RPM,
            the more LEDs would light up, providing a clear and easily readable
            indication of the engine's performance. This LED Car RPM indicator
            not only enhances the aesthetic appeal of the vehicle's interior but
            also serves as a functional tool for drivers to monitor their
            engine's RPM in real-time. By integrating C++, addressable LEDs, and
            leveraging the OBDII port, I have developed an innovative and
            eye-catching solution that combines technology, practicality, and
            style in the world of automotive accessories.
          </p>
          {/* <a
            href="https://github.com/fireclint/crypto-react-firebase"
            target="_blank"
            rel="noreferrer"
          >
            <button className="px-8 py-2 mt-4 mr-8">Code</button>
          </a> */}
          <a
            href="https://cryptobase-yt.web.app/"
            target="_blank"
            rel="noreferrer"
          >
            <button className="px-8 py-2 mt-4">Demo</button>
          </a>
        </div>
        <div className="col-span-4 md:col-span-1 shadow-xl shadow-gray-400 rounded-xl py-4">
          <div className="p-2">
            <p className="text-center font-bold pb-2">Technologies</p>
            <div className="grid grid-cols-3 md:grid-cols-1 ">
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Addressable LEDs
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> C++
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> OBDII Diagnostic Car port
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Algorithms and Data Structures
              </p>
              {/* <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Coin Gecko API
              </p>
              <p className="text-gray-600 py-2 flex items-center">
                <RiRadioButtonFill className="pr-1" /> Routes
              </p> */}
            </div>
          </div>
        </div>
        <Link href="/#projects">
          <p className="underline cursor-pointer">Back</p>
        </Link>
      </div>
    </div>
  );
};

export default crypto;
